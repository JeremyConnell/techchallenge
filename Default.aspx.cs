﻿using System;
using System.Web.Services;

public partial class _Default : System.Web.UI.Page
{
    #region Event Handlers (Form)
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblName.Text = txtName.Text;
        lblAmount.Text = WSServerLogic.NumberToText(txtAmount.Text);
    }
    #endregion


    //Temp Hack - WebServiceReference wasn't behaving
    #region Event Handlers (AJAX)
    [WebMethod]
    public static string NameAndNumToText(string name, string number)
    {
        return WSServerLogic.NameAndNumToText(name, number);
    }
    [WebMethod]
    public static string UnitTests()
    {
        return WSServerLogic.UnitTests();
    }
    #endregion
}