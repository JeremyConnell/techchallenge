﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Services;

/// <summary>
/// Summary description for WSServerLogic
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WSServerLogic : System.Web.Services.WebService
{
    #region Constructor
    public WSServerLogic() { }
    #endregion


    #region Unit Tests
    //I'm familiar with the Dependency Injection pattern, but find it quite messy and intrusive, tending towards lots of big constructors, but perhaps these were just poor implementations. I would go with static factory methods, with a switching parameter, plus parameterless overload, returning the interface
    //A team needs one guy who can set up the unit testing framework once, but beyond that they're all pretty much the same, with a mix of Asserts, and some sort of mock-data facility
    //Both of the above are considered overkill for what is essentially two presentation methods (one sourced externally), plus one input-parsing method, so have gone with a simple visual feedback
    //* TODO: 
    //1. coverage for input filter and combination with name (break out into seperate methods)
    //2. Complete numbers 1-20, test much bigger numbers, 
    [WebMethod]
    public static string UnitTests() 
    {
        var refData = new Dictionary<double, string>() {
            { 1.0, "ONE DOLLAR"},
            { 2.0, "TWO DOLLARS"},
            { 13, "THIRTEEN DOLLARS"},
            { 14.12, "FOURTEEN DOLLARS AND TWELVE CENTS"},
        };

        foreach (var i in refData)
        {
            var test = NumberToText(i.Key);
            if (test != i.Value)
                return string.Concat("Error: {", i.Key, "}: '", test, "' != '", i.Value, "'");
        }
        return string.Concat(refData.Count, " tests passed");
    }
    #endregion


    #region WebMethods
    [WebMethod]
    public static string NameAndNumToText(string name, string number)
    {
        return string.Concat(name, "<br/>", NumToText(number));
    }
    [WebMethod]
    public static string NumToText(string s)
    {
        return WSServerLogic.NumberToText(s);
    }
    #endregion


    #region NumberToText Logic (static)
    //Input Filters (parse double)
    public static string NumberToText(string s)
    {
        //Remove breaking chars
        s = s.Replace("$", "");
        s = s.Replace(",", "");

        //Parse number
        double d = 0;
        if (!double.TryParse(s, out d))
            return "Error: Failed to parse '" + s + "' as money";

        //Main Logic
        return NumberToText(d);
    }

    //Main Logic
    public static string NumberToText(double d)
    {
        //Modulo math
        int full = Convert.ToInt32(d * 100);
        int cents = full % 100;
        int dollars = (full - cents) / 100;

        //Convert to String
        string strDollars = NumberToText(dollars);
        string strCents   = NumberToText(cents);

        //Conditional Pairing of $ and c
        var sb = new StringBuilder();
        if (strDollars.Length > 0)
        {
            sb.Append(strDollars.Trim());
            if (dollars > 1)
                sb.Append(" dollars");
            else
                sb.Append(" dollar");
        }
        if (strCents.Length > 0)
        {
            sb.Append(" and ");
            sb.Append(strCents.Trim());
            if (cents > 1)
                sb.Append(" cents");
            else
                sb.Append(" cent");
        }

        //UPPERCASE (as per spec)
        return sb.ToString()
            .ToUpper();
    }

    //Google searched (external: https://stackoverflow.com/questions/794663/net-convert-number-to-string-representation-1-to-one-2-to-two-etc)
    public static string NumberToText(int n)
    {
        if (n < 0)
            return "Minus " + NumberToText(-n);
        else if (n == 0)
            return "";
        else if (n <= 19)
            return new string[] {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
         "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
         "Seventeen", "Eighteen", "Nineteen"}[n - 1] + " ";
        else if (n <= 99)
            return new string[] {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy",
         "Eighty", "Ninety"}[n / 10 - 2] + " " + NumberToText(n % 10);
        else if (n <= 199)
            return "One Hundred " + NumberToText(n % 100);
        else if (n <= 999)
            return NumberToText(n / 100) + "Hundreds " + NumberToText(n % 100);
        else if (n <= 1999)
            return "One Thousand " + NumberToText(n % 1000);
        else if (n <= 999999)
            return NumberToText(n / 1000) + "Thousands " + NumberToText(n % 1000);
        else if (n <= 1999999)
            return "One Million " + NumberToText(n % 1000000);
        else if (n <= 999999999)
            return NumberToText(n / 1000000) + "Millions " + NumberToText(n % 1000000);
        else if (n <= 1999999999)
            return "One Billion " + NumberToText(n % 1000000000);
        else
            return NumberToText(n / 1000000000) + "Billions " + NumberToText(n % 1000000000);
    }
    #endregion

}
