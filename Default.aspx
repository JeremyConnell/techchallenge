﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tech Challenge</title>
    <style>
        body         { background-color:lavender;}
        table th     { text-align:left;}
        input.ra     { text-align:right; width:95px; }
        input[type="button"] { width:100px; }
        input[type="submit"] { width:100px; }
        span.smaller { font-size:12px; font-family:'Courier New';}
    </style>
</head>
<body>
    <form id="form1" runat="server">

    <asp:ScriptManager ID="sm" runat="server" EnablePageMethods="true">  
        <Services>
            <asp:ServiceReference Path="~/WSServerLogic.asmx" />  
        </Services>  
    </asp:ScriptManager> 

        <div>
            <h1>Simple Postback</h1>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" Text="John Smith" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmount" runat="server" Text="123.45" CssClass="ra" />
                   </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:right">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
            </table>
            <div>
                <asp:Label ID="lblName" runat="server"  class="smaller"/><Br />
                <asp:Label ID="lblAmount" runat="server"  class="smaller" />
           </div>
        </div>

        <div>
            <h1>AJAX Postback</h1>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtName2" runat="server" Text="John Smith" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmount2" runat="server" Text="123.45" CssClass="ra" />
                   </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:right">
                        <input type="button" id="btnUnitTests" value="Unit Tests" onclick="UnitTests()"  />

                        <input type="button" id="btnSubmit2" value="Submit" onclick="Submit()"  />
                    </td>
                </tr>
            </table>
            <div>
                <span id="lblResults" class="smaller"></span>
            </div>
        </div>

        <script>
            //Event Handlers
            function UnitTests()
            {
                PageMethods.UnitTests(SuccessCallback, FailCallback); 
            }
            function Submit()
            {                
                var name   = document.getElementById('txtName2').value;
                var amount = document.getElementById('txtAmount2').value; 

                PageMethods.NameAndNumToText(name, amount, SuccessCallback, FailCallback);

                //Buggy
                //WSServerLogic.NameAndNumToText(name, amount, SuccessCallback, FailCallback);
            }

            //Reusable callbacks
            function SuccessCallback(result)
            {
                var div = document.getElementById('lblResults');
                div.innerHTML = result;
                //alert(result);
            }
            function FailCallback(error)
            {
                alert("ERROR: " + error);
            }
        </script>
    </form>
</body>
</html>
